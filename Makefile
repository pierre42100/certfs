CC=gcc
CFLAGS= -Wall -pedantic -lpthread -ggdb3 `pkg-config openssl fuse --cflags`
LIBS=`pkg-config openssl fuse --libs` -ldl
DEPS=
OBJ= main.o cert_helper.o strutils.o serial_helper.o files_utils.o

all: cert_fs

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

cert_fs: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

static_cert_fs: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS) -static

clean:
	rm -rf *.o *.a *.exe cert_fs

run: all
	./cert_fs ./storage -d mount

debug:
	valgrind --leak-check=full --show-leak-kinds=all ./cert_fs