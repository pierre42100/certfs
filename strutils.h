/**
 * String utilities
 * 
 * @author Pierre Hubert
 */

#pragma once

/**
 * Concatenate two strings
 */
char* concat(const char * one, const char * two);