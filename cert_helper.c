#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <openssl/err.h>
#include <openssl/x509v3.h>


#include "cert_helper.h"
#include "constants.h"
#include "serial_helper.h"
#include "files_utils.h"

#define FAIL_LOAD_CA() { BIO_free_all(bio); X509_free(*ca_crt); EVP_PKEY_free(*ca_key); return 0;}

int load_ca(const char *ca_path, 
            const char *key_path, 
            EVP_PKEY **ca_key, 
            X509 **ca_crt) {
    
    BIO *bio = NULL;
    *ca_crt = NULL;
    *ca_key = NULL;

    printf("CA cert file: %s\n", ca_path);
    printf("CA key file: %s\n", key_path);

    /* Load public key */
    bio = BIO_new(BIO_s_file());
    if(BIO_read_filename(bio, ca_path) == 0) {
        fprintf(stderr, "Failed to read CA file!\n");
        FAIL_LOAD_CA();
    }

    *ca_crt = PEM_read_bio_X509(bio, NULL, NULL, NULL);
    if(ca_crt == NULL) {
        fprintf(stderr, "Failed to read CA PEM!\n");
        FAIL_LOAD_CA();
    }
    BIO_free_all(bio);

    /* Load private key */
    bio = BIO_new(BIO_s_file());
	if (!BIO_read_filename(bio, key_path)) {
        fprintf(stderr, "Failed to read CA key file!\n") ;
        FAIL_LOAD_CA();
    }

	*ca_key = PEM_read_bio_PrivateKey(bio, NULL, NULL, NULL);
	if (!ca_key) {
        fprintf(stderr, "Failed to read CA key PEM!\n") ;
        FAIL_LOAD_CA();
    }
	BIO_free_all(bio);

    return 1;
}


int generate_key_csr(CertSubj *sub, EVP_PKEY **key, X509_REQ **req) {
	*key = NULL;
	*req = NULL;
	RSA *rsa = NULL;
	BIGNUM *e = NULL;

	*key = EVP_PKEY_new();
	if (!*key) goto err;
	*req = X509_REQ_new();
	if (!*req) goto err;
	rsa = RSA_new();
	if (!rsa) goto err;
	e = BN_new();
	if (!e) goto err;

	BN_set_word(e, 65537);
	if (!RSA_generate_key_ex(rsa, RSA_KEY_BITS, e, NULL)) goto err;
	if (!EVP_PKEY_assign_RSA(*key, rsa)) goto err;

	X509_REQ_set_pubkey(*req, *key);

	/* Set the DN of the request. */
	X509_NAME *name = X509_REQ_get_subject_name(*req);
	X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, (const unsigned char*)sub->C_country, -1, -1, 0);
	X509_NAME_add_entry_by_txt(name, "ST", MBSTRING_ASC, (const unsigned char*)sub->ST_state, -1, -1, 0);
	X509_NAME_add_entry_by_txt(name, "L", MBSTRING_ASC, (const unsigned char*)sub->L_locality, -1, -1, 0);
	X509_NAME_add_entry_by_txt(name, "O", MBSTRING_ASC, (const unsigned char*)sub->O_organization, -1, -1, 0);
	X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC, (const unsigned char*)sub->OU_organization_unit, -1, -1, 0);
	X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, (const unsigned char*)sub->CN_common_name, -1, -1, 0);

	/* Self-sign the request to prove that we posses the key. */
	if (!X509_REQ_sign(*req, *key, EVP_sha256())) goto err;

	BN_free(e);

	return 1;
err:
	EVP_PKEY_free(*key);
	X509_REQ_free(*req);
	RSA_free(rsa);
	BN_free(e);
	return 0;
}

int generate_signed_key_pair(CertSubj *sub, EVP_PKEY *ca_key, X509 *ca_crt, EVP_PKEY **key, X509 **crt)
{
	/* Generate the private key and corresponding CSR. */
	X509_REQ *req = NULL;
	if (!generate_key_csr(sub, key, &req)) {
		fprintf(stderr, "Failed to generate key and/or CSR!\n");
		return 0;
	}

	/* Sign with the CA. */
	*crt = X509_new();
	if (!*crt) goto err;

	X509_set_version(*crt, 2); /* Set version to X509v3 */

	/* Generate random 20 byte serial. */
	long serial = serial_get_next();
    if(serial < 1)
        goto err;
    
    ASN1_INTEGER *serial_integer = ASN1_INTEGER_new();
    ASN1_INTEGER_set(serial_integer, serial);
    X509_set_serialNumber(*crt, serial_integer);
    ASN1_INTEGER_free(serial_integer);

	/* Set issuer to CA's subject. */
	X509_set_issuer_name(*crt, X509_get_subject_name(ca_crt));

	/* Set validity of certificate to 2 years. */
	if (!X509_gmtime_adj(X509_get_notBefore(*crt), 0)
		|| !X509_gmtime_adj(X509_get_notAfter(*crt), (long)2*24*365*3600)) {
		ERR_print_errors_fp(stderr);
		goto err;
	}

	/* Explicitly say that the certificate is not a CA */
	X509V3_CTX ctx;
	X509_EXTENSION *ext;
	X509V3_set_ctx(&ctx, NULL, *crt, req, NULL, 0);
	ext = X509V3_EXT_conf(NULL, &ctx, "basicConstraints", "critical,CA:FALSE");
	X509_add_ext(*crt, ext, -1);
	X509_EXTENSION_free(ext);

	/* Add subject alternative name */
	char target_alt_name[256];
	X509_EXTENSION *ext2;
	sprintf(target_alt_name, "DNS:%s", sub->CN_common_name);
	X509V3_set_ctx(&ctx, NULL, *crt, req, NULL, 0);
	ext2 = X509V3_EXT_conf_nid(NULL, &ctx, NID_subject_alt_name, target_alt_name);
	X509_add_ext(*crt, ext2, -1);
	X509_EXTENSION_free(ext2);

	/* Get the request's subject and just use it (we don't bother checking it since we generated
	 * it ourself). Also take the request's public key. */
	X509_set_subject_name(*crt, X509_REQ_get_subject_name(req));
	EVP_PKEY *req_pubkey = X509_REQ_get_pubkey(req);
	X509_set_pubkey(*crt, req_pubkey);
	EVP_PKEY_free(req_pubkey);

	/* Now perform the actual signing with the CA. */
	if (X509_sign(*crt, ca_key, EVP_sha256()) == 0) goto err;

	X509_REQ_free(req);
	return 1;
err:
	EVP_PKEY_free(*key);
	X509_REQ_free(req);
	X509_free(*crt);
	return 0;
}

void crt_to_pem(X509 *crt, uint8_t **crt_bytes, size_t *crt_size)
{
	/* Convert signed certificate to PEM format. */
	BIO *bio = BIO_new(BIO_s_mem());
	PEM_write_bio_X509(bio, crt);
	*crt_size = BIO_pending(bio);
	*crt_bytes = (uint8_t *)malloc(*crt_size + 1);
	BIO_read(bio, *crt_bytes, *crt_size);
	BIO_free_all(bio);
}

void key_to_pem(EVP_PKEY *key, uint8_t **key_bytes, size_t *key_size)
{
	/* Convert private key to PEM format. */
	BIO *bio = BIO_new(BIO_s_mem());
	PEM_write_bio_PrivateKey(bio, key, NULL, NULL, 0, NULL, NULL);
	*key_size = BIO_pending(bio);
	*key_bytes = (uint8_t *)malloc(*key_size + 1);
	BIO_read(bio, *key_bytes, *key_size);
	BIO_free_all(bio);
}


int is_certificate_expired(const char *path) {
	BIO *bio = NULL;

    /* Load public key */
    bio = BIO_new(BIO_s_file());
    if(BIO_read_filename(bio, path) == 0) {
        fprintf(stderr, "Failed to read cert file!\n");
        BIO_free_all(bio);
		return -1;
    }

    X509 *cert = PEM_read_bio_X509(bio, NULL, NULL, NULL);
    if(cert == NULL) {
        fprintf(stderr, "Failed to read certificate PEM!\n");
        BIO_free_all(bio);
		return -1;
    }

	int i = X509_cmp_current_time(X509_get0_notAfter(cert));

	BIO_free_all(bio);
	X509_free(cert);


	switch(i) {

		// Certificate expired
		case -1:
			return 0;
		
		case 1:
			return 1;

		// Failure
		case 0:
		default:
			return -1;
	}
}

int create_renew_certificate_if_required(const char *storage_dir, const char *cn, EVP_PKEY *ca_key, X509 *ca_crt) {
	
	char cert_dir[MAX_FILES_PATH];
	sprintf(cert_dir, "%s/%s", storage_dir, cn);

	char cert_key_path[MAX_FILES_PATH + 12]; // + 12 to remove buffer overflow warning
	sprintf(cert_key_path, "%s/private.pem", cert_dir);
	
	char cert_crt_path[MAX_FILES_PATH + 12]; // + 12 to remove buffer overflow warning
	sprintf(cert_crt_path, "%s/public.pem", cert_dir);

	// Create target directory if required
	if(!dir_exists(cert_dir)) {
		if(mkdir(cert_dir, 0700) != 0) {
			perror("Create dir: ");
			return 1;
		}
	}

	// Check if the certificate is still valid
	else if(file_exists(cert_key_path) && file_exists(cert_crt_path)) {
		if(is_certificate_expired(cert_crt_path) == 1)
			return 1;
	}
	
	
	// Generate a new certificate
	CertSubj subj = {
		.C_country = "FR",
		.ST_state = "Loire",
		.L_locality = "St-Etienne",
		.O_organization = "",
		.OU_organization_unit = "Security",
	};
	strcpy(subj.CN_common_name, cn);

	EVP_PKEY *key = NULL;
	X509 *crt =  NULL;
	if(generate_signed_key_pair(&subj, ca_key, ca_crt, &key, &crt) == 0) return 0;


	/* Convert key and certificate to PEM format. */
	uint8_t *key_bytes = NULL;
	uint8_t *crt_bytes = NULL;
	size_t key_size = 0;
	size_t crt_size = 0;

	key_to_pem(key, &key_bytes, &key_size);
	crt_to_pem(crt, &crt_bytes, &crt_size);

	/* Serialize the certificates */
	if(file_put_contents(cert_key_path, (char*)key_bytes, key_size) != 1) return 0;
	if(file_put_contents(cert_crt_path, (char*)crt_bytes, crt_size) != 1) return 0;

	return 1;
}