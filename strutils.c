#include <malloc.h>
#include <string.h>
#include <assert.h>

#include "strutils.h"

char* concat(const char * one, const char * two) {
    char *buff = (char*)malloc(strlen(one) + strlen(two) + 1);
    assert(buff != NULL);

    strcpy(buff, one);
    strcpy(strchr(buff, '\0'), two);

    return buff;
}