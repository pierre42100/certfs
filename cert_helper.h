/**
 * RSA manipulation functions
 * 
 * Freely inspired from here :
 * https://github.com/zozs/openssl-sign-by-ca/blob/master/openssl1.1/main.c
 * 
 * @author Pierre Hubert
 */

#pragma once

#include <openssl/pem.h>

/**
 * Load a certification authority
 * 
 * @returns 0 in case of failure / 1 else
 */
int load_ca(const char *ca_path, const char *key_path, EVP_PKEY **ca_key, X509 **ca_crt);

typedef struct CertSubj {
    char C_country[3];
    char ST_state[50];
    char L_locality[50];
    char O_organization[50];
    char OU_organization_unit[50];
    char CN_common_name[100];
} CertSubj;

/**
 * Generate a new pair of CSR +  private key 
 */
int generate_key_csr(CertSubj *sub, EVP_PKEY **key, X509_REQ **req);

/**
 * Generate a new pair of certificate + private key
 */
int generate_signed_key_pair(CertSubj *sub, EVP_PKEY *ca_key, X509 *ca_crt, EVP_PKEY **key, X509 **crt);

/**
 * Turn a certificate into a PEM file
 */
void crt_to_pem(X509 *crt, uint8_t **crt_bytes, size_t *crt_size);

/**
 * Turn a key into a PEM file
 */
void key_to_pem(EVP_PKEY *key, uint8_t **key_bytes, size_t *key_size);

/**
 * Check out whether a certificate is expired or not
 * 
 * @return -1 in case of failure / 0 if certificate is expired / 1 if certificate is valid
 */
int is_certificate_expired(const char *path);

/**
 * Create / renew a certificate for a domain, if 
 * the previous certificate has expired or if there
 * is not any previous certificate.
 * 
 * Create the certificate in the storage_dir directory specified
 * in the command line
 * 
 * @returns 1 = success / 0 = failure
 */
int create_renew_certificate_if_required(const char *storage_dir, const char *cn, EVP_PKEY *ca_key, X509 *crt);