#!/bin/bash
STORAGE=storage
CA_STORAGE=$STORAGE/__CA__

echo "This script will reset storage located at $STORAGE"

# Create directory
rm -r $STORAGE
mkdir $STORAGE

# Create directory for certificate
mkdir $CA_STORAGE
echo "1" >> $CA_STORAGE/index
openssl genrsa -out $CA_STORAGE/CA-key.pem 4096
openssl req -new -key $CA_STORAGE/CA-key.pem -x509 -days 10000 -out $CA_STORAGE/CA-cert.pem -subj "/C=FR/ST=Loire/L=StEtienne/O=Global Security/OU=IT Department/CN=example.com"
