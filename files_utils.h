/**
 * Directory utilities
 * 
 * @author Pierre Hubert
 */

#pragma once

/**
 * Checkout whether a directory exists or not
 * 
 * @returns 1 if the directory exists / 0 else
 */
int dir_exists(const char * path);

/**
 * Check out whether a file exists or not
 * 
 * @returns 1 if the file exists / 0 else
 */
int file_exists(const char * path);

/**
 * Write to a file
 * 
 * @returns 1 = success / 0 = failure
 */
int file_put_contents(const char * dest, const char *data, int len);

/**
 * Read an entire file to memory
 * 
 * @returns 1 = success / 0 = failure
 */
int file_read_to_buff(const char *path, char **buffer, long *len);

/**
 * Get the size of a file
 * 
 * @returns -1 in case of failure
 */
long file_size(const char *path);