#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "files_utils.h"

int dir_exists(const char * path) {
    DIR* dir = opendir(path);
    if (dir) {
        /* Directory exists. */
        closedir(dir);
        return 1;
    } else if (ENOENT == errno) {
        /* Directory does not exist. */
        return 0;
    } else {
        /* opendir() failed for some other reason. */
        perror("Check dir: ");
        return 0;
    }
}

int file_exists(const char * path) {
  struct stat   buffer;   
  return (lstat (path, &buffer) == 0);
}


int file_put_contents(const char * dest, const char *data, int len) {
    int fd = open(dest, O_CREAT | O_TRUNC | O_SYNC | O_WRONLY, 0400);

    if(fd == -1) {
        perror("Failed to open file: ");
        return 0;
    }

    if(write(fd, data, len) == -1) {
        perror("Failed to write file: ");
        return 0;
    }

    if(close(fd) == -1) {
        perror("Failed to close file: ");
        return 0;
    }

    return 1;
}

int file_read_to_buff(const char *path, char **dest_buffer, long *len) {
    int fd = open(path, O_RDONLY);

    if(fd == -1) {
        perror("Open file: ");
        return 0;
    }

    /* Measure the size of file */
    long size = lseek(fd, 0, SEEK_END);

    if(size == -1) {
        perror("lseek");
        close(fd);
        return 0;
    }


    /* Reposition to the begining of the file */
    if(lseek(fd, SEEK_SET, 0) == -1) {
        perror("lseek");
        close(fd);
        return 0;
    }

    /* Read the content of the file */
    char *buff = malloc(size * sizeof(char));

    if(buff == NULL) {
        perror("malloc");
        close(fd);
        return 0;
    }

    if(read(fd, buff, size) != size) {
        perror("Failed to read completely file !");
        close(fd);
        free(buff);
        return 0;
    }

    if(close(fd) != 0) {
        perror("close");
    }

    *dest_buffer = buff;
    *len = size;

    return 1;
}

long file_size(const char *path) {
    int fd = open(path, O_RDONLY);

    if(fd == -1) {
        perror("Open file: ");
        return -1;
    }

    /* Measure the size of file */
    long size = lseek(fd, 0, SEEK_END);

    if(size == -1)
        perror("lseek");

    close(fd);
    return size;
}