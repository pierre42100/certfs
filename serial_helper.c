#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "serial_helper.h"

static char INDEX_PATH[256] = "";

void serial_index_set_path(const char * path) {
    strncpy(INDEX_PATH, path, 255);
}

long serial_get_next() {
    if(INDEX_PATH[0] == '\0') {
        fprintf(stderr, "E/ Did not initialize serial index path!\n");
        return -1;
    }

    /* Open file */
    int fd = open(INDEX_PATH, O_RDWR);

    if(fd < 1) {
        perror("Failed to open index file! ");
        return -1;
    }



    /* Read previous serial */
    unsigned int serial;

    char buff[15];
    for(int i = 14; i >= 0; i--)
        buff[i] = '\0';

    if(read(fd, buff, 15) < 1) {
        perror("Failed to read index file! ");
        close(fd);
        return -1;
    }

    if(sscanf(buff, "%x", &serial) == EOF) {
        perror("Failed to parse index file! ");
        close(fd);
        return -1;
    }



    serial++;



    /* Serialize new value */
    lseek(fd, 0, SEEK_SET);
    int num = sprintf(buff, "%x", serial);
    write(fd, buff, num);

    fsync(fd);
    close(fd);

    return (long)serial;
}