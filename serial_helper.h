/**
 * Certificate serial helper
 * 
 * @author Pierre Hubert
 */

#pragma once

/**
 * Use on program startup to define where to find
 * certificates index
 */
void serial_index_set_path(const char * path);

/**
 * Get new certificate serial & increment index
 */
long serial_get_next();