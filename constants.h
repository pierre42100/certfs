/**
 * Project constants
 * 
 * @author Pierre Hubert
 */

#pragma once

/// CA information
#define CA_DIR "__CA__"
#define CA_KEY_PATH CA_DIR "/CA-key.pem"
#define CA_CERT_PATH CA_DIR "/CA-cert.pem"
#define CA_INDEX_PATH CA_DIR "/index"

/// Generated keys bits count
#define RSA_KEY_BITS 4096

/// Maximum size of files path
#define MAX_FILES_PATH 4096