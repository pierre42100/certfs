/**
 * CertFS : A FUSE file system that automatically
 * manage security certificates
 * 
 * This file is freely inspired from this repo :
 * https://github.com/MaaSTaaR/SSFS
 * 
 * @author Pierre Hubert
 */

#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <fuse/fuse_opt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>


#include "constants.h"
#include "cert_helper.h"
#include "strutils.h"
#include "serial_helper.h"
#include "files_utils.h"

/*
    Static structure to easily store & share
    all the data we need to share 
*/
struct RunData {
    EVP_PKEY *ca_key;
    X509 *ca_crt;

	/**
	 * This field MUST NOT include trailing slash !!!
	 */
    char storage_path[MAX_FILES_PATH + 1];
};

static struct RunData data;


static int do_getattr( const char *path, struct stat *st );
static int do_readdir( const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi );
static int do_read( const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi );

static struct fuse_operations operations = {
    .getattr	= do_getattr,
    .readdir	= do_readdir,
    .read		= do_read,
};

/* Extract storage path from command line */
int parse_arg(void *uselessdata, const char *arg, int key,
			       struct fuse_args *outargs) {
	
	if(data.storage_path[0] != '\0')
		return 1;

	strncpy(data.storage_path, arg, MAX_FILES_PATH);

	if(dir_exists(arg) != 1) {
		fprintf(stderr, "Specified storage directory does not exists!\n");
		return -1;
	}

	return 0;
}

int main(int argc, char **argv)
{
	struct fuse_args args = FUSE_ARGS_INIT (argc, argv);
    int res;

	data.storage_path[0] = '\0';

	if(fuse_opt_parse (&args, NULL, NULL, parse_arg) != 0) {
		fprintf(stderr, "Invalid arguments! (see -h for more information)\n");
		return -1;
	}

	if(data.storage_path[0] == '\0') {
		fprintf(stderr, "Invalid arguments! First argument should be storage path !!!\n");
		return -1;
	}

    /* Load CA certificate */
    data.ca_key = NULL;
    data.ca_crt = NULL;

    char *ca_crt_path = concat(data.storage_path, "/" CA_CERT_PATH);
    char *ca_key_path = concat(data.storage_path, "/" CA_KEY_PATH);
    res = load_ca(ca_crt_path, ca_key_path, &data.ca_key, &data.ca_crt);
    
    free(ca_crt_path);
    free(ca_key_path);
    
    if(res == 0) return -1;


    /* Intialize certificates serial index */
    char *ca_serial_path = concat(data.storage_path, "/" CA_INDEX_PATH);
    serial_index_set_path(ca_serial_path);
    free(ca_serial_path);

    fuse_main(args.argc, args.argv, &operations, NULL);


    /* Free CA */
    EVP_PKEY_free(data.ca_key);
    X509_free(data.ca_crt);

    return 0;
}



/**
 * Check out whether a path is a path for the directory that
 * contains a certificate or not
 * 
 * eg. can we consider a certificate can be created inside this
 * directory
 * 
 * @returns 1 = true / 0 = false
 */
static int is_path_for_certificate_directory(const char *path) {
	
	// No for root certificate / too small paths
	if(strlen(path) < 3 || path[0] != '/') return 0;

	// No for sub directories
	if(strchr(path + 1, '/') != NULL) return 0;

	// No for directories with bad characters
	if(strchr(path, ' ') != NULL ||
		strchr(path, '(') != NULL ||
		strchr(path, '#') != NULL ||
		strchr(path, '\t') != NULL ||
		strchr(path, '\'') != NULL ||
		strchr(path, '"') != NULL ||
		strchr(path, '%') != NULL ||
		strchr(path, '{') != NULL ||
		strchr(path, '}') != NULL ||
		strchr(path, '@') != NULL ||
		strchr(path, '&') != NULL ||
		strchr(path, '$') != NULL ||
		strchr(path, ')') != NULL)
		return 0;

	// No for domains without a point
	if(strchr(path, '.') == NULL) return 0;

	// Check if path starts with a '.'
	if(path[1] == '.') return 0;

	// Block autorun.inf file
	if(strcmp("/autorun.inf", path) == 0) return 0;

	return 1;
}


/**
 * Get the real path of a file (from the mounted file system
 * to the real filesystem)
 * 
 * @returns 1 = success / 0 = failure
 */
static int get_real_path_for_file(const char *fs_path, char *real_path) {
	
	if(strstr(fs_path, "/intermediate.pem") != NULL) 
		sprintf(real_path, "%s/%s", data.storage_path, CA_CERT_PATH);
	
	else
		sprintf(real_path, "%s%s", data.storage_path, fs_path);
	return 1;
}

static int do_getattr( const char *path, struct stat *st )
{
	printf("[getattr] Attributes of %s requested\n", path);
	
	// GNU's definitions of the attributes (http://www.gnu.org/software/libc/manual/html_node/Attribute-Meanings.html):
	// 		st_uid: 	The user ID of the file’s owner.
	//		st_gid: 	The group ID of the file.
	//		st_atime: 	This is the last access time for the file.
	//		st_mtime: 	This is the time of the last modification to the contents of the file.
	//		st_mode: 	Specifies the mode of the file. This includes file type information (see Testing File Type) and the file permission bits (see Permission Bits).
	//		st_nlink: 	The number of hard links to the file. This count keeps track of how many directories have entries for this file. If the count is ever decremented to zero, then the file itself is discarded as soon 
	//						as no process still holds it open. Symbolic links are not counted in the total.
	//		st_size:	This specifies the size of a regular file in bytes. For files that are really devices this field isn’t usually meaningful. For symbolic links this specifies the length of the file name the link refers to.
	
	st->st_uid = getuid(); // The owner of the file/directory is the user who mounted the filesystem
	st->st_gid = getgid(); // The group of the file/directory is the same as the group of the user who mounted the filesystem
	st->st_atime = time( NULL ); // The last "a"ccess of the file/directory is right now
	st->st_mtime = time( NULL ); // The last "m"odification of the file/directory is right now
	
	if ( strcmp( path, "/" ) == 0 )
	{
		st->st_mode = S_IFDIR | 0400;
		st->st_nlink = 2; // Why "two" hardlinks instead of "one"? The answer is here: http://unix.stackexchange.com/a/101536
	}

	// We check if we are getting information about a certificate
	else if(
		is_path_for_certificate_directory(path) == 1
		&& create_renew_certificate_if_required(data.storage_path, path + 1, data.ca_key, data.ca_crt) == 1) {
		st->st_mode = S_IFDIR | 0400;
		st->st_nlink = 2;
	}

	else
	{
		st->st_mode = S_IFREG | 0644;
		st->st_nlink = 1;
		
		char real_file_path[MAX_FILES_PATH];
		if(get_real_path_for_file(path, real_file_path) != 1) {
			fprintf(stderr, "Could not determine real path for file: %s!", path);
			return -1;
		}
		long size = file_size(real_file_path);
		if(size == -1) {
			fprintf(stderr, "Could not get file size: %s!", path);
			return -1;
		}

		st->st_size = size;
	}
	
	return 0;
}

static int do_readdir( const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi )
{
	printf( "--> Getting The List of Files of %s\n", path );
	
	filler( buffer, ".", NULL, 0 ); // Current Directory
	filler( buffer, "..", NULL, 0 ); // Parent Directory
	
	if ( strcmp( path, "/" ) == 0 ) // If the user is trying to show the files/directories of the root directory show the following
	{
		DIR *dir = opendir(data.storage_path);
		if(dir == NULL) {
			perror("opendir: ");
			return 1;
		}

		struct dirent *direntry;
		while((direntry = readdir(dir)) != NULL) {
			// Ignore __CA__ directory
			if(strcmp(CA_DIR, direntry->d_name) != 0
				&& strcmp(".", direntry->d_name) != 0
				&& strcmp("..", direntry->d_name) != 0)
				filler( buffer, direntry->d_name, NULL, 0);
		}

		if(closedir(dir) != 0) {
			perror("closedir: ");
			return 1;
		}
	}

	/* Otherwise we show the three files : public.pem, private.pem, intermediate.pem */
	else {
		filler( buffer, "public.pem", NULL, 0 );
		filler( buffer, "private.pem", NULL, 0 );
		filler( buffer, "intermediate.pem", NULL, 0 );
	}
	
	return 0;
}

static int do_read( const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi )
{
	printf( "--> Trying to read %s, %lu, %lu\n", path, offset, size );

	char file[MAX_FILES_PATH];
	if(get_real_path_for_file(path, file) != 1)
		return -1;

	if(file_exists(file) != 1)
		return -1;

	char *buff;
	long file_size;

	if(file_read_to_buff(file, &buff, &file_size) != 1) {
		return -1;
	}
	
	memcpy( buffer, buff + offset, file_size - offset );
	
	free(buff);

	return file_size - offset;
}